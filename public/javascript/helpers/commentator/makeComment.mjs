export default function makeComment(message) {
  const commentContainer = document.getElementById("comment_text");
  
  setTimeout(() => {
    commentContainer.innerText = message;
  }, 500) 
}