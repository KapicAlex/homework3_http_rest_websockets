export default async function fetchData (id, textBlock, roomName, socket) {
  await fetch(`/game/texts/${id}`)
          .then(res => res.json())
          .then(result => {
            renderTextInput (result, textBlock, roomName, socket);
          })
}

function renderTextInput (result, textBlock, roomName, socket) {
  let newText = '';
  let text = result;
  textBlock.innerHTML = `<span id="new_text">${newText}</span><div id="text-container">${text}</div>`;
  const persent = 100 / text.length;

  const onKeyUp = e => {
    if(e.key === text[0]){
      newText += text[0];
      text = text.substring(1) ?? ''
      textBlock.innerHTML = `<span id="new_text">${newText}</span><div id="text-container">${text}</div>`;
      let progress = persent * newText.length;
      socket.emit("GAME_PROGRESS", {roomName, progress});
      if(text.lenght === 30) socket.emit("ON_FINISH_LINE", roomName);
    }
  }
  
  window.addEventListener("keyup", onKeyUp)
}