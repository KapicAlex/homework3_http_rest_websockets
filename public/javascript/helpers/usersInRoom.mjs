export default function usersInRoomRender ({room}, username, quitFromRoom, onReady) {
  const roomInfo = document.getElementById("roomInfo");
  const infoHtml = `<h1 class="title">ROOM -- ${room.roomName}</h1>
                    <button id="quit-room-btn">Back To Rooms</button>`
  roomInfo.insertAdjacentHTML('beforeend', infoHtml);

  const btnQuit = document.getElementById("quit-room-btn");
  btnQuit.addEventListener("click", ()=>quitFromRoom(room.roomName, room));

  const btnReady = document.getElementById("ready-btn");
  btnReady.onclick = () => onReady(room.roomName, username);

  const info = usersInfoRender(room?.usersList, username);
  roomInfo.appendChild(info);
}

function usersInfoRender(roomUsers, username) {
  const div = document.createElement("div");
  div.className = "users-progress"
  let progressHtml = '';

  roomUsers.forEach(user => {
    progressHtml += `<span id="led_${user.username}" class="ready-status-${user.isReady ? "green" : "red"}"></span>
                    <span class="username">${user.username} ${user.username === username ? '(you)' : ''}</span>
                    <div class="progress-line"><div class="user-progress user-progress-${user.username}" style="width: ${user.progress}%"></div>
                    </div>`
  });

  div.innerHTML = progressHtml;

  return div;
}