import * as config from "./config";
import { checkedRooms, allReadyInRoom } from "./helpers/checkRoom"
import startGame from "./helpers/startGame"
import Commentator from "./services/commentator";

const usersList = new Set();
const rooms = new Map;

export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;
    const commentator = new Commentator(io, socket, rooms);

    if(!usersList.has(username)) {

      usersList.add(username);
      io.to(socket.id).emit("ROOMS_UPDATE", checkedRooms(rooms));

    }else{
      socket.emit("USER_IS_ACTIVE", username);

    }

    const createRoom = roomName => {
      if(!rooms.has(roomName)){

        rooms.set(roomName, {roomName, usersList: []});
        connectToRoom(roomName);

      }else{
        socket.emit("ROOM_IS_ACTIVE", roomName);
      }
    };

    const connectToRoom = roomName => {
      socket.join(roomName, () => {
        const userInfo = {
          username,
          progress: 0,
          isReady: false,
        }

        const room = rooms.get(roomName);
        rooms.set(roomName, {roomName: roomName, usersList: [...room?.usersList, userInfo], winnersList: []});

        socket.emit("CONNECT_ROOM_DONE", {room: rooms.get(roomName)});
        socket.broadcast.emit("ROOMS_UPDATE", checkedRooms(rooms));
        io.to(roomName).emit("ROOM_UPDATE", {room: rooms.get(roomName)});
        commentator.greetUser(rooms.get(roomName), roomName);
      })
    }

    const quitFromRoom = roomName => {
      socket.leave(roomName, () => {
        const room = rooms.get(roomName);

        room?.usersList.forEach(user => {
          if(user.username === username) user.isReady = !user.isReady;
        })

        if(room?.usersList.length === 1) {

          rooms.delete(roomName);
          io.to(socket.id).emit("QUIT_ROOM_DONE", checkedRooms(rooms));
          socket.broadcast.emit("ROOMS_UPDATE", checkedRooms(rooms));

        }else{

          const newUsersList = room?.usersList.filter(user => user.username !== username);
          room.usersList = newUsersList;
          rooms.set(roomName, room);

          io.to(socket.id).emit("QUIT_ROOM_DONE", checkedRooms(rooms));
          io.to(roomName).emit("ROOM_UPDATE", {room: rooms.get(roomName)});

          setTimeout(() => {commentator.onUserQuit(roomName)}, 0);
        }
      })
    }

    const readyOn = roomName => {
      const room = rooms.get(roomName);

      room?.usersList.forEach(user => {
        if (user.username === username) {

          user.isReady = !user.isReady;
          setTimeout(() => {commentator.onReadyUser(roomName, user)}, 0);

        }
      });

      if(allReadyInRoom(room)) {

        setTimeout(() => {commentator.onReadyAllUsers(roomName, rooms.get(roomName))}, 0);
        startGame(roomName, io, rooms, commentator);

      }

      rooms.set(roomName, room);

      io.to(roomName).emit("ROOM_UPDATE", {room: rooms.get(roomName)});
      socket.broadcast.emit("ROOMS_UPDATE", checkedRooms(rooms));
    }

    const gameProgress = ({roomName, progress}) => {
      io.to(roomName).emit("PROGRESS_UPDATE", {progress, username});
      const room = rooms.get(roomName);

      room?.usersList.forEach(user => {
        if(user.username === username) {
          user.progress = progress;
        }
      });
      
      rooms.set(roomName, room);

      if(progress === 100) {
        room.winnersList.push(username);
        rooms.set(roomName, room);

        const winners = room?.winnersList.length;
        const users = room?.usersList.length;
        
        if(winners === users) {
          io.to(roomName).emit("GAME_OVER", roomName);
        }else if(winners > 1) {
          commentator.onFinishAnothertUsers(roomName);
        }
      }
    }

    const newGame = (roomName) => {
      const room = rooms.get(roomName);
      room.winnersList = [];

      room?.usersList.forEach(user => {
        user.isReady = false;
        user.progress = 0;
      });

      rooms.set(roomName, room);

      io.to(roomName).emit("ROOM_UPDATE", {room: rooms.get(roomName)});
      socket.broadcast.emit("ROOMS_UPDATE", checkedRooms(rooms));
    }

    const onFinishLine = (roomName) => {
      commentator.onFinishLine(roomName);
    }

    const fullDisconnect = () => {
      rooms.forEach(room => {
        quitFromRoom(room.roomName);
      });

      usersList.delete(username);
    }

    socket.on("disconnect", fullDisconnect);
    socket.on("ROOM_CREATE", createRoom);
    socket.on("ROOM_CONNECT", connectToRoom);
    socket.on("QUIT_ROOM", quitFromRoom);
    socket.on("READY_ON", readyOn);
    socket.on("GAME_PROGRESS", gameProgress);
    socket.on("NEW_GAME", newGame);
    socket.on("ON_FINISH_LINE", onFinishLine)

  });
};



