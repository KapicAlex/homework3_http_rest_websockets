export const activity = {
  greetingDefault: function () {
    return `Мене звати Містер Бот\n
            і я коментуватиму цю чудову подію.`
  },

  joinFirstUser: function (username) {
    return `Вітаю першого гравця - ${username}!\n
            Залишається дочекатися інших.`;
  },

  joinAnotherUsers: function (username) {
    return `Вітаю наступного гравця ${username}!\n
                  ${username} вже зайняв своє місце на старті.`;
  },

  onReadyUser: function (username) {
    return `${username} вже рветься в бій.\n
            Однак інші не поспішають.`;
  },

  onDiscardReady: function (username) {
    const text = `Що трапилось з ${username}?\n
            Куди це він?\n`;
    const variableMessage = [
      "Зрозуміло. Забув улюблений картуз",
      "а-а-а...поклик природи. Хвилювання то таке діло...",
      "Грає на нерви іншим?",
      "Тю, невже ключі забув?",
      "Ну, поки почекаємо.",
      "Здається якась несправність..."
    ];
    const random = Math.floor(Math.random() * variableMessage.length);

    return text + variableMessage[random];
  },

  onUserQuit: function (username) {
    return `Ой лишенько! ${username} нас залишив.\n
            Що ж, залишаються тільки найвідважніші.`
  },

  onReadyAllUsers: function (users) {
    const vehicles = [
      "Lamborgini",
      "Ferrari",
      "Maseratti",
      "Bugatti",
      "Harley-Davidson",
      "Alfa romeo",
      "Таврії",
      "Козі діда Панаса",
      "Ступі Баби Яги",
      "Лісапеді тітки Марічки"
    ];
    let text = `Зворотній відлік почато!\n
                    І так, на стартовій лініі:\n`;
    const startData = {};

    users.forEach(user => {
      const random = Math.floor(Math.random() * vehicles.length);
      text += `\n${user.username} на ${vehicles[random]}`;
      
      startData[user.username] = vehicles[random];
    });

    text += "\nХай переможе найкращий!";

    return [text, startData];
  },

  onStartGame: function () {
    return `СТАРТ!!!`;
  },

  onThirtySeconds: function (users, restOfTime, gameTimer) {
    let text = `Пройшло всього ${gameTimer} секунд,\n
                    а здається вічність. Тож маємо наступне:`;

    users.forEach((user, idx) => {
      switch (idx) {
        case 0: text += `\n${user.username} лідирує,`;
          break;
        case 1: text += `\nдругим мчить ${user.username}`;
          break;
        case 2: text += `\nне полишає надій та замикає лідируючу трійку ${user.username}!`;
          break;
        default: return;
      }
    });

    text += `\nЗалишається ще ${restOfTime} секунд.
                \nВсе ще може змінитися!`;

    return text;
  },

  onFinishFirstUser: function (username) {
    return `Ого-го! ${username} прийшов до фінішу першим.`;
  },
  
  onFinishAnothertUsers: function (username) {
    return `${username} фінішував.`;
  },

  onFinishLine: function (username) {
    return `${username} вже на фінішній прямій!`;
  },

  endGame: function (users, startData, time) {
    let text = `Все! Кінець!\n
                Перегони закінчились за ${time} секунд\n
                  Кращі з кращих на перегонах:\n`;
    
    users.forEach((user, idx) => {
      if (idx < 3) {
        text += `№${idx + 1} ⮞ ${user} на ${startData[user]}\n`;
      }
    });

    return text;
  },

  newGame: function () {
    return "Хто бажає повторити цей шалений драйв?";
  },

  randomComment: function () {
    let text = "Народна мудрість:\n";
    const tales = [
      "Життя прожити — не поле перейти.",
      "День довгий, а вік короткий.",
      "Життя біжить — як музика дзвенить.",
      "Ніхто не знає, що його в житті чекає.",
      "Хто живому не дав, той хай і мертвому не дає.",
      "Без труда нема плода.",
      "Битому собаці кия не показуй.",
      "Бачить кіт сало, та сили мало.",
      "В закритий рот муха не влізе.",
      "В ліс дрова не возять.",
      "Є в глечику молоко, та голова не влізе.",
      "Орел мух не ловить.",
    ];
    const random = Math.floor(Math.random() * tales.length);

    return text += tales[random];
  }
}